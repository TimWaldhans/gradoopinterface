package Interface.Exceptions;


/**
 * Exception thrown and to be handled upon reading a empty Graph
 */

public class EmptyGraphException extends Exception{

    public EmptyGraphException(String message){
        super(message);
    }

}

package Interface.Neo4j;


import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

/**
 * Class for light weight handling Gradoop and Apache Flink environments
 */


public class GradoopConfig {

    private final String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    private  ExecutionEnvironment env;
    private  GradoopFlinkConfig config;


    public GradoopConfig(){
        this.env = ExecutionEnvironment.getExecutionEnvironment();
        this.config = GradoopFlinkConfig.createConfig(env);

    }


    /**
     * Prints the Graph associated with this environment
     * @param logicalGraph
     */

    public  void printGraph (LogicalGraph logicalGraph){
        try{
            logicalGraph.print();
        }catch (Exception e){

        }
    }

    public String getCYPHER_SOURCE_ID() {
        return CYPHER_SOURCE_ID;
    }

    public ExecutionEnvironment getEnv() {
        return env;
    }

    public void setEnv(ExecutionEnvironment env) {
        this.env = env;
    }

    public GradoopFlinkConfig getConfig() {
        return config;
    }

    public void setConfig(GradoopFlinkConfig config) {
        this.config = config;
    }
}

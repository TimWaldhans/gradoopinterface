package Interface.Neo4j.Remote;


import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Neo4jInterface;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.*;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.types.Node;
import org.neo4j.driver.v1.types.Relationship;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class operates on the Remote Neo4j API (bolt protocol) to read a Graph from
 * Neo4j into the Gradoop format.
 */


public class RemoteReader extends Interface.Neo4j.Reader implements Neo4jInterface {

    private Driver driver;
    private static LinkedList<ImportVertex<String>> vertices;


    /**
     * Initiates a DB Connection for this class
     * @param adress    Database URI
     * @param user      Username for DB connection
     * @param password  Password for DB connection
     */
    public RemoteReader(String adress, String user, String password){

        Config config = Config.build().withoutEncryption().toConfig();

        this.driver = GraphDatabase.driver(adress, AuthTokens.basic(user, password),config);
    }
    /**
     * Reads a Cypher Query String and returns a set of Vertices in GradoopConfig Format
     * @return List of ImportVertex
     */
    private List<ImportVertex<String>> getNodesForQuery(List<Node> nodes ){


        LinkedList<ImportVertex<String>> importVertices = new LinkedList<>();
        for(Node node : nodes){
            String id = String.valueOf(node.id());
            String label = createLabel(node);
            Properties properties = Properties.createFromMap(node.asMap());
            properties.set(CYPHER_SOURCE_ID,String.valueOf(node.id()));
            importVertices.add(new ImportVertex<String>(id,label,properties));
        }

        return importVertices;
    }
    /**
     * Reads a Cypher Query String and returns a set of Edges in GradoopConfig Format
     * @return List of ImportEdge
     */
    private  List<ImportEdge<String>> getEdgesForQuery(List<Relationship> relationships ){

        LinkedList<ImportEdge<String>> importEdges = new LinkedList<>();

        for(Relationship relationship :relationships){
            String id = String.valueOf(relationship.id());

            String sourceId = String.valueOf(relationship.startNodeId());
            String targetId = String.valueOf(relationship.endNodeId());
            Properties properties = Properties.createFromMap(relationship.asMap());
            properties.set(CYPHER_SOURCE_NODE,sourceId);
            properties.set(CYPHER_TARGET_NODE,targetId);
            properties.set(CYPHER_SOURCE_ID,String.valueOf(relationship.id()));
            String label = relationship.type();

            importEdges.add(new ImportEdge<String>(id,sourceId,targetId,label,properties));

        }


        return importEdges;

    }

    /**
     * Extracts the Set of Nodes out of a Cypherquery
     * @param query the cypher query String
     * @return Returns the Node Object representation
     */
    private List[] extractElements(String query){
       StatementResult result = getResultForQuery(query);
        List<Node> nodes = new LinkedList<>();
       HashMap<Long,Node> added = new HashMap(result.keys().size());
       List<Relationship> relationships = new LinkedList<>();
       while (result.hasNext()){

           List<Value> values = result.next().values();
           for(Value value :values){

              try{
                  if (value.type().name()=="NODE") {
                      if(added.get(value.asNode().id())==null) {
                          Node curr = value.asNode();
                          nodes.add(curr);
                          added.put((Long) curr.id(),curr);
                      }
                  }
                  if(value.type().name().equals("RELATIONSHIP")) {
                      relationships.add(value.asRelationship());
                  }

              }catch (Exception e){
                  continue;
              }

           }
       }
        List[] res = {nodes,relationships};
       return res;

    }



    /**
     * Extracts a Logical Graph in GradoopConfig Format
     * @param query the cypher query String
     * @return Returns the Logical Graph
     */
    public LogicalGraph getGraphFromNeo4j(String query, GradoopConfig gradoopInterface){

        List[] queryResult = extractElements(query);
        LinkedList<ImportVertex<String>> vertices = (LinkedList) getNodesForQuery(queryResult[0]);
        queryResult[0] = null;
        LinkedList<ImportEdge<String>> edges = (LinkedList) getEdgesForQuery(queryResult[1]);
        queryResult[1] = null;
        System.gc();

        if (edges.size() == 0) {
            edges = null;
        }
        if (vertices.size() == 0) {
            LogicalGraphFactory graphFac = gradoopInterface.getConfig().getLogicalGraphFactory();
            LogicalGraph lg = graphFac.createEmptyGraph();
            return lg;

        }


        LogicalGraph myGraph = getLogicalGraphFromCollections(vertices,edges,gradoopInterface);

        return myGraph;

    }

    /**
     * Creates a Graph from a List of Vertices and Edges
     * @param vertices List of Vertices
     * @param edges List of Edges
     * @param gradoopInterface GradoopConfig Context
     */
    private LogicalGraph getLogicalGraphFromCollections(LinkedList<ImportVertex<String>> vertices, LinkedList<ImportEdge<String>> edges, GradoopConfig gradoopInterface){


        LinkedList<EPGMVertex> resultVertices = new LinkedList<>();
        ///home/timtpc/.IdeaIC2017.2/config/idea64.vmoptions
        LinkedList<EPGMEdge> resultEdges = new LinkedList<>();
        HashMap<String,ImportVertex<String>> vertexTreeMap = hashVerticesByID(vertices);
        HashMap<String,EPGMVertex> added = new HashMap<>(vertices.size());


        LogicalGraphFactory graphFac = gradoopInterface.getConfig().getLogicalGraphFactory();
        EPGMVertexFactory vertexFactory = (EPGMVertexFactory) graphFac.getVertexFactory();
        EPGMEdgeFactory edgeFactory = (EPGMEdgeFactory) graphFac.getEdgeFactory();
        EPGMGraphHead head = graphFac.getGraphHeadFactory().createGraphHead();
        GradoopIdSet idSet = new GradoopIdSet();
        idSet.add(head.getId());
        if(edges != null) {
            EPGMVertex epgmTargetVertex;
            EPGMVertex epgmSourceVertex;
            for (ImportEdge edge : edges) {


                if (added.get(edge.getSourceId().toString()) == null) {

                    ImportVertex<String> sourceVertex = vertexTreeMap.get(edge.getSourceId().toString());
                    epgmSourceVertex = createVertexFromNode(sourceVertex, vertexFactory, idSet);
                    added.put(edge.getSourceId().toString(), epgmSourceVertex);
                    resultVertices.add(epgmSourceVertex);
                    vertexTreeMap.remove(edge.getSourceId().toString());
                } else {
                    epgmSourceVertex = added.get(edge.getSourceId().toString());

                }

                if (added.get(edge.getTargetId().toString()) == null) {
                    ImportVertex<String> targetVertex = vertexTreeMap.get(edge.getTargetId().toString());
                    epgmTargetVertex = createVertexFromNode(targetVertex, vertexFactory, idSet);
                    added.put(edge.getTargetId().toString(), epgmTargetVertex);
                    resultVertices.add(epgmTargetVertex);
                    vertexTreeMap.remove(edge.getTargetId().toString());

                } else {
                    epgmTargetVertex = added.get(edge.getTargetId().toString());

                }




                EPGMEdge epgmEdge = createEdgesFromNeo4j(edge, epgmSourceVertex.getId(), epgmTargetVertex.getId(), edgeFactory, idSet);

                resultEdges.add(epgmEdge);



            }
        }


        for(Map.Entry<String,ImportVertex<String>> entry : vertexTreeMap.entrySet()){

            resultVertices.add(createVertexFromNode(entry.getValue(),vertexFactory,idSet));
        }


        LogicalGraph result = graphFac.fromCollections(head,resultVertices,resultEdges);
        return result;

    }


    /**
     * Extracts a Logical Graph in GradoopConfig Format
     * @return Returns the Logical Graph representing the whole Database
     */

    public LogicalGraph getGraphFromNeo4j(GradoopConfig gradoopInterface){
        try {
            return getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Takes a Cypher query and returns the Result Object
     * @param query The Cypher Query String
     * @return The StatementResult Object
     */
    protected StatementResult getResultForQuery(String query){

        try(Session session = driver.session()){

            StatementResult res = session.run(query);
            return res;

        }catch(Exception e){
            e.printStackTrace();

        }
        return null;
    }


    /**
     * Closes current DB connection
     */

    public void close()
    {
        this.driver.close();
    }


    public Driver getDriver() {
        return driver;
    }

}

package Interface.Neo4j;


public interface Neo4jInterface {

    /***
     * This Interface is a marker interface which also delivers basic strings often used for marking origin
     * ID'S of cypher
     */
    String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
    String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";

}

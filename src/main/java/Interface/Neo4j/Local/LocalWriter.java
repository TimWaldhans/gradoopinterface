package Interface.Neo4j.Local;


import Interface.Neo4j.Neo4jInterface;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.Property;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class operates on the Local Neo4j API to write
 * a Graph on the Gradoop format into the Neo4j Database
 */

public class LocalWriter  extends Interface.Neo4j.Writer  implements Neo4jInterface {


    private GraphDatabaseService graphDatabase;

    /**
     * Constructor for creating a Local Writer instance
     * @param path Path to the Interface.Neo4j .db Directory
     */
    public LocalWriter(String path){

        GraphDatabaseFactory graphDBFactory = new GraphDatabaseFactory();
        this.graphDatabase = graphDBFactory.newEmbeddedDatabase(
                new File(path));

    }

    /**
     * Constructor for creating a Local Writer instance, if more than one
     * Reader object is desired. Pass the Graphdatabase Objecto From another
     * Reader or Writer instance to this constructor
     * @param gdb Path to the Interface.Neo4j .db Directory
     * @see LocalReader
     */

    public LocalWriter(GraphDatabaseService gdb) {

        this.graphDatabase = gdb;

    }

    /**
     * Computes the differences between two Logical Graphs in their Properties
     * and Elements and writes them back to the Interface.Neo4j Database associated with this
     * Object
     * @param left LogicalGraph before transformatio
     * @param right LogicalGraph after transformation
     *              @see LogicalGraph
     */

    public void putGraph(LogicalGraph left, LogicalGraph right){

        deleteElements(left,right);
        updateElements(right);
        addElements(right);

    }


    /**
     * Deletes Elements which are not preserved after the transformation
     * @param left LogicalGraph before Transformation
     * @param right LogicalGraph before Transformation
     * @see  LogicalGraph
     */

    public   void deleteElements(LogicalGraph left, LogicalGraph right){

        try{
            Transaction tx = graphDatabase.beginTx();
            List<EPGMVertex> leftVertices = left.getVertices().collect();
            List<EPGMVertex> rightVertices = right.getVertices().collect();
            List<EPGMEdge> leftEdges = left.getEdges().collect();
            List<EPGMEdge> rightEdges = right.getEdges().collect();
            HashMap<String,EPGMEdge> edgeMap = hashEdgesByCypherID(rightEdges);
            HashMap<String,EPGMVertex> vMap = hashVerticesByCypherID(rightVertices);

            for(EPGMVertex leftVert : leftVertices){
                EPGMVertex preserved = vMap.get(leftVert.getPropertyValue(CYPHER_SOURCE_ID).toString());
                if(preserved == null) {
                    String id = leftVert.getPropertyValue(CYPHER_SOURCE_ID).toString();
                    graphDatabase.execute("MATCH (n) WHERE id(n)=" + id + " DETACH DELETE n");

                }
            }
            for(EPGMEdge eLeft: leftEdges){
                EPGMEdge preserved = edgeMap.get(eLeft.getPropertyValue(CYPHER_SOURCE_ID).toString());
                if(preserved == null ){
                    String id = eLeft.getPropertyValue(CYPHER_SOURCE_ID).toString();
                    graphDatabase.execute("MATCH ()-[e]->() WHERE id(e)="+id+" DELETE e");
                }

            }

            tx.success();
            tx.close();

        }catch(Exception e){
            e.printStackTrace();
        }

    }


    /**
     * Sets the new Properties which changed during the transformation
     * @param right LogicalGraph after Transformation
     * @see LogicalGraph
     */

    public void updateElements(LogicalGraph right){

        Transaction tx = graphDatabase.beginTx();
        //Zu erhaltendes, zu updatendes

        try {
            List<EPGMEdge> edges = right.getEdges().collect();
            List<EPGMVertex> vertices = right.getVertices().collect();

            for(EPGMVertex vertex : vertices){

                if(!vertex.hasProperty(CYPHER_SOURCE_ID)){continue;}
                String id = vertex.getPropertyValue(CYPHER_SOURCE_ID).toString();
                vertex.removeProperty(CYPHER_SOURCE_ID);
                Iterator<Property> props = vertex.getProperties().iterator();
                String label = createLabelExpression(vertex);
                graphDatabase.execute("MATCH (n) WHERE id(n)="+id+" SET n:"+label);
                graphDatabase.execute("MATCH (n) WHERE id(n)="+id+" SET n = { }");
                while(props.hasNext()){
                    Property property = props.next();
                    String key = property.getKey();
                    PropertyValue value = property.getValue();
                    if(value.getType().equals(String.class)) {
                        graphDatabase.execute("MATCH (n) WHERE id(n)=" + id + " SET n." + key + "='" + value + "'");
                    }else{
                        graphDatabase.execute("MATCH (n) WHERE id(n)=" + id + " SET n." + key + "= " + value);
                    }

                }
            }
            for(EPGMEdge edge : edges){
                if(!edge.hasProperty(CYPHER_SOURCE_ID)){continue;}
                String id = edge.getPropertyValue(CYPHER_SOURCE_ID).toString();
                edge.removeProperty(CYPHER_SOURCE_ID);
                edge.removeProperty(CYPHER_TARGET_NODE);
                edge.removeProperty(CYPHER_SOURCE_NODE);
                Iterator<Property> props = edge.getProperties().iterator();
                graphDatabase.execute("MATCH ()-[e]-() WHERE id(e)="+id+" SET e={ } ");
                while(props.hasNext()){
                    Property property = props.next();
                    String key = property.getKey();
                    PropertyValue value = property.getValue();
                    try {
                        if(value.getType().equals(String.class)) {
                            String statement = "MATCH ()-[e]-() WHERE id(e)=" + id + " SET e." + key + "= '" + value + "'";
                            graphDatabase.execute(statement);
                        }else{
                            String statement = "MATCH ()-[e]-() WHERE id(e)=" + id + " SET e." + key + "= " + value;
                            graphDatabase.execute(statement);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }finally {
                        continue;
                    }

                }

            }


            tx.success();
            tx.close();


        }catch(Exception e){
            e.printStackTrace();
        }


    }



    /**
     * Ads the new Properties which changed during the transformation
     * @param right LogicalGraph nach Transformation
     * @see LogicalGraph
     */

    public  void addElements(LogicalGraph right){

        Transaction tx = graphDatabase.beginTx();
        try {

            List<EPGMVertex> vertices = right.getVertices().collect();
            List<EPGMEdge> newEdges = right.getEdges().collect();
            HashMap<GradoopId, EPGMVertex> vertexMap = hashVerticesByGradoopID(vertices);
            HashMap<GradoopId, EPGMVertex> added = new HashMap<>(vertices.size());
            GradoopIdSet dummyIdSet = new GradoopIdSet();
            //Exception, wenn GraphID's von right nicht übereinstimmen. Vorher verify rufen

            for(EPGMEdge edgeToAdd:newEdges){

                boolean sourceNotAdded = added.get(edgeToAdd.getSourceId())==null;
                boolean targetNotadded = added.get(edgeToAdd.getTargetId())==null;


                if(edgeToAdd.hasProperty(CYPHER_SOURCE_NODE)){

                    if(edgeToAdd.hasProperty(CYPHER_TARGET_NODE)){
                        //nichts tun, da bereits in Datenbank. Bei geänderten Eigenschaften bereits geupdatet.
                        continue;
                    }
                    if(!edgeToAdd.hasProperty(CYPHER_TARGET_NODE) && sourceNotAdded){
                        // Kante hat bereits SourceKnoten in Datenbank, aber keinen TargetKnoten
                        //SourceKnoten in "added" mit resultierender ID abspeichern, um zu merken
                        Map<String,Object> property = new HashMap<>();
                        property.put(CYPHER_SOURCE_ID,edgeToAdd.getPropertyValue(CYPHER_SOURCE_NODE));
                        EPGMVertex dummy = new EPGMVertex(edgeToAdd.getSourceId()," ", Properties.createFromMap(property),dummyIdSet);
                        added.put(dummy.getId(),dummy);
                        sourceNotAdded = false;
                        addEdgeWithTargetVertex(edgeToAdd,tx,added,vertexMap);
                        continue;
                    }
                }
                if(!edgeToAdd.hasProperty(CYPHER_SOURCE_NODE)){


                    if(edgeToAdd.hasProperty(CYPHER_TARGET_NODE) && targetNotadded){
                        //Kante hat bereits TargetNode
                        Map<String,Object> property = new HashMap<>();
                        property.put(CYPHER_SOURCE_ID,edgeToAdd.getPropertyValue(CYPHER_TARGET_NODE));
                        EPGMVertex dummy = new EPGMVertex(edgeToAdd.getTargetId()," ",Properties.createFromMap(property),dummyIdSet);
                        added.put(dummy.getId(),dummy);
                        targetNotadded = false;
                        addEdgeWithSourceVertex(edgeToAdd,tx,added,vertexMap);
                        continue;
                    }
                    if(!edgeToAdd.hasProperty(CYPHER_TARGET_NODE)){
                        // Kante ist komplett neu hat keine Cypher Source Id's
                        // beide Knoten in in "added" mit resultierender ID abspeichern, um zu merken
                        // Wenn einer der Knoten bereits in "added" ist, mit anderen Funktionen fortfahren!


                        if(vertexMap.get(edgeToAdd.getSourceId()).hasProperty(CYPHER_SOURCE_ID)){
                            sourceNotAdded = false;
                            added.put(edgeToAdd.getSourceId(),vertexMap.get(edgeToAdd.getSourceId()));
                        }
                        if(vertexMap.get(edgeToAdd.getTargetId()).hasProperty(CYPHER_SOURCE_ID)){
                            targetNotadded = false;
                            added.put(edgeToAdd.getTargetId(),vertexMap.get(edgeToAdd.getTargetId()));
                        }
                        if(!sourceNotAdded && !targetNotadded) {
                            // Source und Target sind bereits hinzugefügt worden
                            sourceNotAdded = false;
                            targetNotadded = false;
                            addEdgeWithOldVertices(edgeToAdd,tx,added);

                        }
                        if(sourceNotAdded && !targetNotadded) {
                            // Nur Target ist bereits hinzugefügt worden
                            sourceNotAdded = false;
                            addEdgeWithSourceVertex(edgeToAdd,tx,added,vertexMap);

                        }
                        if(!sourceNotAdded && targetNotadded ) {
                            // Nur Source ist bereits hinzugefügt worden
                            targetNotadded = false;
                            addEdgeWithTargetVertex(edgeToAdd,tx,added,vertexMap);

                        }
                        if(targetNotadded && sourceNotAdded) {
                            // Noch kein Knoten der Kante ist bereits hinzugefügt worden
                            targetNotadded = false;
                            sourceNotAdded = false;
                            EPGMVertex source = vertexMap.get(edgeToAdd.getSourceId());
                            EPGMVertex target = vertexMap.get(edgeToAdd.getTargetId());
                            addEdgeWithNewVertices(edgeToAdd, tx, source, target,added);

                        }
                        continue;
                    }

                }

            }
            for(EPGMVertex vertex : vertices){

                if(added.get(vertex.getId())!= null){continue;}
                if(!vertex.hasProperty(CYPHER_SOURCE_ID)) {
                    String vlabel = createLabelExpression(vertex);
                    Map<String, Object> params = new HashMap<>();
                    if(vertex.getProperties()!= null) {
                        params.put("PROPERTIES_NODE", propertiesToMap(vertex.getProperties()));
                        String query = "CREATE (n:" + vlabel + " $PROPERTIES_NODE)";
                       graphDatabase.execute(query, params);
                    }else{
                        String query = "CREATE (n:" + vlabel +" )";
                        graphDatabase.execute(query);
                    }
                }

            }


            tx.success();
            tx.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Adds a New Edge to this Database, in case the Target node does not yet exist in the target Database
     * @param edgeToAdd The Edge about to be added
     * @param tx The current Transaction
     * @param added Already added Vertices
     * @param vertexTreeMap The Vertex TreeMap for fatser search
     */
    private void addEdgeWithTargetVertex(EPGMEdge edgeToAdd, Transaction tx,
                                         HashMap<GradoopId,EPGMVertex> added, HashMap<GradoopId,EPGMVertex> vertexTreeMap){

        EPGMVertex target = vertexTreeMap.get(edgeToAdd.getTargetId());
        String sourceId = added.get(edgeToAdd.getSourceId()).getPropertyValue(CYPHER_SOURCE_ID).toString();
        String targetLabel = createLabelExpression(target);
        String edgeLabel = edgeToAdd.getLabel();

        Map<String, Object> params = new HashMap<>();
        String query = "";
        if(edgeToAdd.getProperties()!=null) {
            params.put("PROPERTIES_EDGE", propertiesToMap(edgeToAdd.getProperties()));
            params.put("PROPERTIES_TARGET_NODE", propertiesToMap(target.getProperties()));

            query = "MATCH (n) WHERE id(n)=" + sourceId + " CREATE (n)-[e:" + edgeLabel + " $PROPERTIES_EDGE]->" +
                    "(m:" + targetLabel + " $PROPERTIES_TARGET_NODE) RETURN e";
            // hier die keys der hinzugefuegten Knoten mit speichern in "added", um später zu verknüpfen
        }else{

            params.put("PROPERTIES_TARGET_NODE", propertiesToMap(target.getProperties()));
            if(target.getProperties()!=null) {
                query = "MATCH (n) WHERE id(n)=" + sourceId + " CREATE (n)-[e:" + edgeLabel + " ]->" +
                        "(m:" + targetLabel + " $PROPERTIES_TARGET_NODE) RETURN e";
            }else{
                query = "MATCH (n) WHERE id(n)=" + sourceId + " CREATE (n)-[e:" + edgeLabel + " ]->" +
                        "(m:" + targetLabel + " ) RETURN e";
            }
        }

        Result result =  graphDatabase.execute(query,params);
        Map<String,Object> row = result.next();
        while(result.hasNext()) {
            for (String key : result.columns()) {
                if (row.get(key) instanceof Relationship) {
                    target.setProperty(CYPHER_SOURCE_ID,((Relationship) row.get(key)).getEndNodeId());
                }

            }
        }
        added.put(target.getId(),target);

    }


    /**
     * Adds a New Edge to this Database, in case the Source node does not yet exist in the target Database
     * @param edgeToAdd The Edge about to be added
     * @param tx The current Transaction
     * @param added Already added Vertices
     * @param vertexTreeMap The Vertex TreeMap for fatser search
     */
    private void addEdgeWithSourceVertex(EPGMEdge edgeToAdd, Transaction tx,
                                         HashMap<GradoopId,EPGMVertex> added, HashMap<GradoopId,EPGMVertex> vertexTreeMap){

        EPGMVertex source = vertexTreeMap.get(edgeToAdd.getSourceId());
        String targetId = added.get(edgeToAdd.getTargetId()).getPropertyValue(CYPHER_SOURCE_ID).toString();
        String sourceLabel = createLabelExpression(source);
        String edgeLabel = edgeToAdd.getLabel();

        Map<String, Object> params = new HashMap<>();
        String query = null;
        if(edgeToAdd.getProperties()!=null) {
            params.put("PROPERTIES_EDGE", propertiesToMap(edgeToAdd.getProperties()));
            params.put("PROPERTIES_SOURCE_NODE", propertiesToMap(source.getProperties()));

            query = "MATCH (m) WHERE id(m)=" + targetId + " CREATE (n:" + sourceLabel + " $PROPERTIES_SOURCE_NODE )-[e:" + edgeLabel + " $PROPERTIES_EDGE]->" +
                    "(m) RETURN e";
        }else{

            if(source.getProperties() != null) {
                params.put("PROPERTIES_SOURCE_NODE", propertiesToMap(source.getProperties()));

                query = "MATCH (m) WHERE id(m)=" + targetId + " CREATE (n:" + sourceLabel + " $PROPERTIES_SOURCE_NODE )-[e:" + edgeLabel + "]->" +
                        "(m) RETURN e";
            }else{
                query = "MATCH (m) WHERE id(m)=" + targetId + " CREATE (n:" + sourceLabel + " )-[e:" + edgeLabel + "]->" +
                        "(m) RETURN e";
            }

        }

        // hier die keys der hinzugefuegten Knoten mit speichern in "added", um später zu verknüpfen

        Result result =  graphDatabase.execute(query,params);
        Map<String,Object> row = result.next();
        while(result.hasNext()) {
            for (String key : result.columns()) {
                if (row.get(key) instanceof Relationship) {
                    source.setProperty(CYPHER_SOURCE_ID,((Relationship) row.get(key)).getStartNodeId());
                }

            }
        }
        added.put(source.getId(), source);


    }


    /**
     * Adds a New Edge to this Database, in case the Target and Source node do not yet exist in the GradoopConfig Database
     * @param edgeToAdd The Edge about to be added
     * @param tx The current Transaction
     * @param added Already added Vertices
     */
    private void addEdgeWithOldVertices(EPGMEdge edgeToAdd, Transaction tx, HashMap<GradoopId, EPGMVertex> added){

        EPGMVertex source = added.get(edgeToAdd.getSourceId());
        EPGMVertex target = added.get(edgeToAdd.getTargetId());
        String sourceId = source.getPropertyValue(CYPHER_SOURCE_ID).toString();
        String targetId = target.getPropertyValue(CYPHER_SOURCE_ID).toString();
        String edgeLabel = edgeToAdd.getLabel();
        Map<String, Object> params = new HashMap<>();
        if(edgeToAdd.getProperties()!=null) {
            params.put("PROPERTIES_EDGE", propertiesToMap(edgeToAdd.getProperties()));

            graphDatabase.execute("MATCH (n),(m) WHERE id(n)=" + sourceId + " AND id(m)=" + targetId +
                    " CREATE (n)-[e:" + edgeLabel + " $PROPERTIES_EDGE]->(m)", params);
        }else{
            graphDatabase.execute("MATCH (n),(m) WHERE id(n)=" + sourceId + " AND id(m)=" + targetId +
                    " CREATE (n)-[e:" + edgeLabel + " ]->(m)");
        }

    }
    /**
     * Adds a New Edge to this Database, in case the nodes do not yet exist in the GradoopConfig Database
     * @param tx The current Transaction
     * @param added Already added Vertices
     */
    private  void addEdgeWithNewVertices(EPGMEdge edge, Transaction tx, EPGMVertex source,
                                         EPGMVertex target,
                                         HashMap<GradoopId, EPGMVertex> added){

        String sourceLabel = createLabelExpression(source);
        String targetLabel = createLabelExpression(target);
        String edgeLabel = edge.getLabel();

        Map<String, Object> params = new HashMap<>();
        String query ="";
        if(edge.getProperties()!=null) {
            params.put("PROPERTIES_EDGE", propertiesToMap(edge.getProperties()));
            params.put("PROPERTIES_SOURCE_NODE", propertiesToMap(source.getProperties()));
            params.put("PROPERTIES_TARGET_NODE", propertiesToMap(target.getProperties()));

            query = "CREATE (n:" + sourceLabel + " $PROPERTIES_SOURCE_NODE )-[e:" + edgeLabel + " $PROPERTIES_EDGE]->" +
                    "(m:" + targetLabel + " $PROPERTIES_TARGET_NODE) RETURN e";
        }else{
            params.put("PROPERTIES_SOURCE_NODE", propertiesToMap(source.getProperties()));
            params.put("PROPERTIES_TARGET_NODE", propertiesToMap(target.getProperties()));
            if(source.getProperties() != null && target.getProperties()!= null) {
                query = "CREATE (n:" + sourceLabel + " $PROPERTIES_SOURCE_NODE )-[e:" + edgeLabel + "]->" +
                        "(m:" + targetLabel + " $PROPERTIES_TARGET_NODE) RETURN e";
            }else{
                if(source.getProperties() == null && target.getProperties()!= null){
                    query = "CREATE (n:" + sourceLabel + " )-[e:" + edgeLabel + "]->" +
                            "(m:" + targetLabel + " $PROPERTIES_TARGET_NODE) RETURN e";
                }
                if(source.getProperties() != null && target.getProperties()== null){
                    query = "CREATE (n:" + sourceLabel + " )-[e:" + edgeLabel + "]->" +
                            "(m:" + targetLabel + " ) RETURN e";
                }
            }
        }



        Result result = graphDatabase.execute(query,params);
        Map<String,Object> row = result.next();
        while(result.hasNext()) {
            for (String key : result.columns()) {
                if (row.get(key) instanceof Relationship) {
                    source.setProperty(CYPHER_SOURCE_ID,((Relationship) row.get(key)).getStartNodeId());
                    target.setProperty(CYPHER_SOURCE_ID,((Relationship) row.get(key)).getEndNodeId());
                }

            }
        }


        added.put(target.getId(), target);
        added.put(source.getId(), source);

    }

    /**
     * Closes current Connection to the Database
     */

    public void close(){
        graphDatabase.shutdown();
    }

    public  GraphDatabaseService getGraphDatabase() {
        return graphDatabase;
    }



}

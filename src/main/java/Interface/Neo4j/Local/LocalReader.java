package Interface.Neo4j.Local;

import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Neo4jInterface;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;
import java.util.*;

/**
 * This class operates on the Local Neo4j API to read a Graph from
 * Neo4j into the Gradoop format.
 */

public class LocalReader extends Interface.Neo4j.Reader implements Neo4jInterface {


    private GraphDatabaseService graphDatabase;


    /**
     * Constructor for creating a Local Reader instance
     * @param path Path to the Interface.Neo4j .db Directory
     */
    public LocalReader(String path) {

        GraphDatabaseFactory graphDBFactory = new GraphDatabaseFactory();
        this.graphDatabase = graphDBFactory.newEmbeddedDatabase(
                new File(path));

    }
    /**
     * Constructor for creating a Local Reader instance, if more than one
     * Reader object is desired. Pass the Graphdatabase Objecto From another
     * Reader or Writer instance to this constructor
     * @param gdb Path to the Interface.Neo4j .db Directory
     * @see LocalWriter
     */
    public LocalReader(GraphDatabaseService gdb) {

       this.graphDatabase = gdb;

    }

    /**
     * Returns a LogicalGraph object as Gradoop Instance for a given Cypher Query in this Reader
     * Object
     * @param cypherQuery Cypher Query String
     * @return Resulting Query Graph
     */
    public LogicalGraph getGraphFromNeo4j(String cypherQuery, GradoopConfig gradoop){

        Result result = graphDatabase.execute(cypherQuery);
        LinkedList[] elements = getElementsForQuery(result);
        LinkedList<ImportVertex<String>> vertices = elements[0];
        elements[0] = null;
        LinkedList<ImportEdge<String>> edges = elements[1];
        elements[1] = null;
        System.gc();

        // creates a Empty Graph if no Nodes are found

        if (vertices.size() == 0) {
            LogicalGraphFactory graphFac = gradoop.getConfig().getLogicalGraphFactory();
            LogicalGraph lg = graphFac.createEmptyGraph();
            return lg;

        }

        LogicalGraph myGraph = getLogicalGraphFromCollections(vertices,edges,gradoop);

        return myGraph;


    }


    /**
     * Creates a Graph from a List of Vertices and Edges read from
     * a Interface.Neo4j Database
     * @param vertices List of Vertices
     * @param edges List of Edges
     * @param gradoopInterface GradoopConfig Context
     * @see  ImportVertex
     * @see ImportEdge
     */

    private LogicalGraph getLogicalGraphFromCollections(LinkedList<ImportVertex<String>> vertices, LinkedList<ImportEdge<String>> edges, GradoopConfig gradoopInterface){

        LinkedList<EPGMVertex> resultVertices = new LinkedList<>();
        LinkedList<EPGMEdge> resultEdges = new LinkedList<>();
        HashMap<String,ImportVertex<String>> vertexTreeMap = hashVerticesByID(vertices);
        HashMap<String,EPGMVertex> added = new HashMap<>(vertices.size());


        LogicalGraphFactory graphFac = gradoopInterface.getConfig().getLogicalGraphFactory();
        EPGMVertexFactory vertexFactory = (EPGMVertexFactory) graphFac.getVertexFactory();
        EPGMEdgeFactory edgeFactory = (EPGMEdgeFactory) graphFac.getEdgeFactory();
        GradoopIdSet idSet = new GradoopIdSet();

        EPGMVertex epgmTargetVertex;
        EPGMVertex epgmSourceVertex;

        for(ImportEdge edge : edges){


            // here, for every Edge, a few checks are done to assure that
            // a Vertex with multiple Edges is not added multiple times when the edge
            // is read in


            if(added.get(edge.getSourceId())==null) {
                ImportVertex<String> sourceVertex = vertexTreeMap.get(edge.getSourceId().toString());
                epgmSourceVertex = createVertexFromNode(sourceVertex, vertexFactory, idSet);
                added.put(edge.getSourceId().toString(),epgmSourceVertex);
                vertices.remove(sourceVertex);
                resultVertices.add(epgmSourceVertex);
            }else{
                epgmSourceVertex = added.get(edge.getSourceId());
                ImportVertex<String> sourceVertex = vertexTreeMap.get(edge.getSourceId().toString());
                vertices.remove(sourceVertex);
            }

            if(added.get(edge.getTargetId())==null) {
                ImportVertex<String> targetVertex = vertexTreeMap.get(edge.getTargetId().toString());
                epgmTargetVertex = createVertexFromNode(targetVertex, vertexFactory, idSet);
                added.put(edge.getTargetId().toString(),epgmTargetVertex);
                vertices.remove(targetVertex);
                resultVertices.add(epgmTargetVertex);

            }else{
                epgmTargetVertex = added.get(edge.getTargetId());
                ImportVertex<String> targetVertex = vertexTreeMap.get(edge.getTargetId().toString());
                vertices.remove(targetVertex);
            }


            EPGMEdge epgmEdge =createEdgesFromNeo4j(edge,epgmSourceVertex.getId(),epgmTargetVertex.getId(),edgeFactory,idSet);

            resultEdges.add(epgmEdge);

        }


        for(ImportVertex<String> vertex : vertices){
            resultVertices.add(createVertexFromNode(vertex,vertexFactory,idSet));
        }


        LogicalGraph result = graphFac.fromCollections(resultVertices,resultEdges);
        return result;

    }


    /**
     * Reads in a List of Gradoop Vertices and Edges for further computation
     * @param result A LinkedList with the first argument as Vertices and the second argument as
     *               Edges
     * @return The resulting set of edges and vertices
     */

    private LinkedList[] getElementsForQuery(Result result){

        LinkedList<ImportEdge<String>> edges = new LinkedList<ImportEdge<String>>();
        LinkedList<ImportVertex<String>> vertices = new LinkedList<ImportVertex<String>>();
        Set<String> idSet1 = new HashSet<String>();
        Set<String> idSet2 = new HashSet<String>();

        while (result.hasNext()){
            Map<String,Object> row = result.next();
            for(String key: result.columns()){
                if(row.get(key) instanceof Relationship){
                    String id = String.valueOf(((Relationship) row.get(key)).getId());
                    if(idSet1.contains(id)){continue;}
                    idSet1.add(id);
                    Transaction tx = graphDatabase.beginTx();
                    String sourceId = String.valueOf(((Relationship) row.get(key)).getStartNodeId());
                    String targetId = String.valueOf(((Relationship) row.get(key)).getEndNodeId());
                    String label = ((Relationship) row.get(key)).getType().name();
                    Properties properties = Properties.createFromMap(((Relationship) row.get(key)).getAllProperties());
                    tx.success();
                    properties.set(CYPHER_SOURCE_NODE,sourceId);
                    properties.set(CYPHER_TARGET_NODE,targetId);
                    properties.set(CYPHER_SOURCE_ID,id);
                    edges.add(new ImportEdge<String>(id,sourceId,targetId,label,properties));

                }
                if(row.get(key) instanceof Node){
                    String id = String.valueOf(((Node) row.get(key)).getId());
                    if(idSet2.contains(id)){continue;}
                    idSet2.add(id);
                    Transaction tx = graphDatabase.beginTx();
                    String label = createLabel((Node) row.get(key));
                    tx.success();
                    Properties properties = Properties.createFromMap(((Node) row.get(key)).getAllProperties());
                    properties.set(CYPHER_SOURCE_ID,id);
                    vertices.add(new ImportVertex<String>(id,label,properties));
                }

            }
        }

        LinkedList[] res = {vertices,edges};
        return res;

    }

    public void close(){
        graphDatabase.shutdown();
    }


    /**
     * Gives out the underlying Neo4j API database service
     * @return The GraphDatabaseService Object
     */

    public GraphDatabaseService getGraphDatabase() {
        return graphDatabase;
    }
}








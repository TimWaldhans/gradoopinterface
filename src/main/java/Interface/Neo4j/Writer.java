package Interface.Neo4j;

import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.Property;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This Class implements basic writer operations for inheriting writer Classes
 */
public abstract class Writer implements Neo4jInterface {



    /**
     * Creates a TreeMap for faster search in VertexSets, the key is the source Cypher ID
     * @param edges The List of EPGMVertex
     * @return TreeMap of the EPGM Vertices
     * @see  EPGMEdge
     */
     protected  HashMap<String,EPGMEdge> hashEdgesByCypherID(List<EPGMEdge> edges){

        HashMap<String,EPGMEdge> map = new HashMap<>(edges.size());
        for(EPGMEdge edge : edges){
            if(edge.hasProperty(CYPHER_SOURCE_ID)){
                map.put(edge.getPropertyValue(CYPHER_SOURCE_ID).toString(),edge);
            }
        }
        return map;
    }

    /**
     * Creates a TreeMap for faster search in VertexSets, the key is the source Cypher ID
     * @param vertices The List of EPGMVertex
     * @return TreeMap of the EPGM Vertices
     * @see EPGMVertex
     */
    protected  HashMap<String,EPGMVertex> hashVerticesByCypherID(List<EPGMVertex> vertices){

        HashMap<String,EPGMVertex> map = new HashMap<>(vertices.size());
        for(EPGMVertex vertex: vertices){
            if(vertex.hasProperty(CYPHER_SOURCE_ID)){
                map.put(vertex.getPropertyValue(CYPHER_SOURCE_ID).toString(),vertex);
            }
        }
        return map;
    }

    /**
     * Creates a TreeMap for faster search in VertexSets, the key is the source GradoopConfig ID
     * @param vertices The List of EPGMVertex
     * @return TreeMap of the EPGM Vertices
     * @see EPGMVertex
     */
     protected HashMap<GradoopId,EPGMVertex> hashVerticesByGradoopID(List<EPGMVertex> vertices){

        HashMap<GradoopId,EPGMVertex> map = new HashMap<>(vertices.size());

        vertices.forEach(v-> map.put(v.getId(),v));
        return map;
    }

    /**
     * Creates a HashMap representation of GradoopConfig Properties
     * @param properties The GradoopConfig Properties Object
     * @return The Map containing key value pairs
     * @see Properties
     */
    protected   Map<String,Object> propertiesToMap(Properties properties){


        Map<String,Object> map = new HashMap<>();
        Iterator<Property> props = properties.iterator();
        while(props.hasNext()) {
            Property current = props.next();
            if(current.getKey().toString().equals(CYPHER_SOURCE_ID) ||
                    current.getKey().toString().equals(CYPHER_TARGET_NODE) ||
                    current.getKey().toString().equals(CYPHER_SOURCE_NODE))
            {continue;}


                map.put(current.getKey().toString(), current.getValue().getObject());

        }
        return map;

    }

    /**
     * Creates a matching Labelexpression for multiple Labels, splits by empty spaces
     * @param source Vertex to extract the labels from
     * @return CraetionString for Labelset
     * @see EPGMVertex
     */

    protected String createLabelExpression(EPGMVertex source){

        String label = "";
        String[] input = source.getLabel().split("\\s+");
        if (input.length==1){
            return input[0];
        }
        else{
            label = input[0];
            for(int i = 1 ; i< input.length; i++){
                label += ":"+input[i];
            }
        }

        return label;


    }

    public  abstract void close();

    /**
     * Computes the differences between two Logical Graphs in their Properties
     * and Elements and writes them back to the Interface.Neo4j Database associated with this
     * Object
     * @param left LogicalGraph before transformatio
     * @param right LogicalGraph after transformation
     *              @see LogicalGraph
     */

    public abstract void putGraph(LogicalGraph left, LogicalGraph right);




}

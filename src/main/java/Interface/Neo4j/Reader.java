package Interface.Neo4j;


import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.neo4j.driver.v1.types.Node;
import org.neo4j.graphdb.Label;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * This Class implements basic reader operations for inheriting writer Classes
 */

public abstract class Reader implements Neo4jInterface{


    /**
     * Creates a HashTree for further faster search in Vertices
     * @param vertices The List of Vertices to Hash
     * @return The given Hashtree
     */

    protected HashMap<String,ImportVertex<String>> hashVerticesByID(LinkedList<ImportVertex<String>> vertices){
        HashMap<String,ImportVertex<String>> map = new HashMap<>(vertices.size());
        vertices.forEach(v-> map.put(v.getId().toString(),v));
        return map;
    }

    /**
     * Converts a ImportVertex to a EPGMVertex
     * @param node   Edge to convert
     * @param fac       EPGM Edge Factory
     * @param idSet     Graph Id set
     * @return
     */

    protected EPGMVertex createVertexFromNode(ImportVertex<String> node, EPGMVertexFactory fac, GradoopIdSet idSet){



        EPGMVertex resultVertex = fac.initVertex(GradoopId.get(),node.getLabel(),node.getProperties(), idSet);

        return resultVertex;
    }

    /**
     * Converts a ImportEdge to a EPGMEdge
     * @param edge    Edge to convert
     * @param sourceId Source GradoopConfig Id
     * @param targetId Target GradoopConfig Id
     * @param fac       EPGM Edge Factory
     * @param idSet     Graph Id set
     * @return
     */

    protected EPGMEdge createEdgesFromNeo4j(ImportEdge<String> edge, GradoopId sourceId, GradoopId targetId, EPGMEdgeFactory fac, GradoopIdSet idSet){

        EPGMEdge resultEdge = fac.initEdge(GradoopId.get(),edge.getLabel(),sourceId,targetId,edge.getProperties(),idSet);

        return resultEdge;
    }


    /**
     * Creates a Label and Splits multiple labels in one String, if necessary
     * @param node The Node with the Label to extract
     * @return
     */
    protected String createLabel(Node node){

        Iterator<String> iterator = node.labels().iterator();
        String label = iterator.next();
        while(iterator.hasNext()){
            label += " " + iterator.next();
        };
        return label;

    }

    /**
     * Creates a Label and Splits multiple labels in one String, if necessary
     * @param node The Node with the Label to extract
     * @return
     */

    protected String createLabel(org.neo4j.graphdb.Node node){

        Iterator<Label> iterator = node.getLabels().iterator();
        String label = iterator.next().name();
        while(iterator.hasNext()){
            label += " " + iterator.next().name();
        }

        return label;

    }


    public abstract LogicalGraph getGraphFromNeo4j(String query, GradoopConfig gradoopInterface);

    public abstract void close();




}

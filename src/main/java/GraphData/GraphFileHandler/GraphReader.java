package GraphData.GraphFileHandler;

import Interface.Neo4j.GradoopConfig;
import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.Property;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

/**
 * Graph Reader is a current workaround for reading Graphs out of
 * File containing the.gdl format
 */

public class GraphReader {


    private static String escape = "%%%%%ESCAPED%%%%%";


    /**
     * Reads a Local File in the .gdl Format into a Logical Graph Object
     *
     * @param path   Path to file
     * @param config Underlying Gradoop Environment
     * @return The Logical Graph defined in the File
     * @see LogicalGraph
     * @see org.gradoop.flink.io.impl.gdl.GDLEncoder
     */


    public static LogicalGraph readGraphGDL(String path, GradoopConfig config) {


        try {
            File file = new File(path);

            BufferedReader br = new BufferedReader(new FileReader(file));

            String graphString = "";
            String st;
            while ((st = br.readLine()) != null) {
                graphString += st + "\n";
            }
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(config.getConfig());
            loader.initDatabaseFromString(graphString);

            if (loader.getGraphHeads().size() == 1) {
                LogicalGraph lg = loader.getLogicalGraph();
                List<EPGMEdge> edges = lg.getEdges().collect();
                List<EPGMVertex> verts = lg.getVertices().collect();
                // Here, the Escape Character for ' " ' is converted back to  ' " '
                for (EPGMVertex v : verts) {
                    Properties p = v.getProperties();
                    if(p==null){continue;}
                    for (Property prop : p) {
                        if (prop.getValue().isString()) {
                            char c = '"';
                            String newString = prop.getValue().toString().replaceAll(escape, String.valueOf(c));
                            PropertyValue pNew = PropertyValue.create(newString);
                            v.setProperty(prop.getKey(), pNew);
                        }
                    }

                }
                for (EPGMEdge e : edges) {
                    Properties p = e.getProperties();
                    if(p==null){continue;}
                    for (Property prop : p) {
                        if (prop.getValue().isString()) {
                            char c = '"';
                            String newString = prop.getValue().toString().replaceAll(escape, String.valueOf(c));
                            PropertyValue pNew = PropertyValue.create(newString);
                            e.setProperty(prop.getKey(), pNew);
                        }
                    }
                }

                LogicalGraphFactory graphFac = (LogicalGraphFactory) lg.getFactory();
                EPGMGraphHead head = lg.getGraphHead().collect().get(0);
                head.setLabel(lg.getGraphHead().collect().get(0).getLabel());
                GradoopIdSet idSet = new GradoopIdSet();
                idSet.add(head.getId());
                lg = graphFac.fromCollections(head, verts, edges);
                return lg;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

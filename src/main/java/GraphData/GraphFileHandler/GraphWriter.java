package GraphData.GraphFileHandler;


import org.gradoop.common.model.impl.id.GradoopIdSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.Property;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.io.impl.gdl.GDLEncoder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Workaround class for saving Graphs in their GDL representation into a file
 */

public class GraphWriter {

    private static String escape = "%%%%%ESCAPED%%%%%";


    /**
     * Writes a Logical Graph to a persistent file in the .gdl Format.
     * Note, that the " character in the Graph Properties Elements is temporarily converted to a escape character, since
     * the .gdl Creator does not allow this character in Properties
     * @param lg The Logical Graph object to be written
     * @param path Path to save the File to
     * @param fileName The Filename
     * @see GDLEncoder
     */


    public static void writeGraphGDL(LogicalGraph lg,String path, String fileName){


        try {
            path += "/"+fileName+".gdl";

            List<EPGMEdge> edges = new LinkedList<>();

            try {
                 edges = lg.getEdges().collect();
            }catch(Exception e){
                e.printStackTrace();
            }
            finally {
                List<EPGMVertex> verts = lg.getVertices().collect();


                // convertes the " character to a escape String
                for (EPGMVertex v : verts) {
                    Properties p = v.getProperties();
                    if (p == null) {
                        continue;
                    }
                    for (Property prop : p) {
                        if (prop.getValue().isString()) {
                            char c = '"';
                            String newString = prop.getValue().toString().replaceAll(String.valueOf(c), escape);
                            PropertyValue pNew = PropertyValue.create(newString);
                            v.setProperty(prop.getKey(), pNew);
                        }
                    }

                }
                if (edges.size() > 0) {
                    for (EPGMEdge e : edges) {
                        Properties p = e.getProperties();
                        if (p == null) {
                            continue;
                        }
                        for (Property prop : p) {
                            if (prop.getValue().isString()) {
                                char c = '"';
                                String newString = prop.getValue().toString().replaceAll(String.valueOf(c), escape);
                                PropertyValue pNew = PropertyValue.create(newString);
                                e.setProperty(prop.getKey(), pNew);
                            }
                        }
                    }
                }
                LogicalGraphFactory graphFac = (LogicalGraphFactory) lg.getFactory();
                EPGMGraphHead head = lg.getGraphHead().collect().get(0);
                head.setLabel(fileName);
                GradoopIdSet idSet = new GradoopIdSet();
                idSet.add(head.getId());
                lg = graphFac.fromCollections(head, verts, edges);


                GDLEncoder enc = new GDLEncoder(lg.getGraphHead().collect(),
                        lg.getVertices().collect(), lg.getEdges().collect());

                String gdl = enc.getGDLString();
                FileOutputStream fos = new FileOutputStream(path);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
                bw.write(gdl);
                bw.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }


}


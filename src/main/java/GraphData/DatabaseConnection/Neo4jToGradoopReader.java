package GraphData.DatabaseConnection;



import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Local.LocalReader;
import Interface.Neo4j.Reader;
import Interface.Neo4j.Remote.RemoteReader;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * This Class encapsulates the Local and Remote Reader Classes for better handling
 * as a interface.
 * For using this API, encapsulating communication with this class is recommended
 *
 */

public class Neo4jToGradoopReader {


    // Underlying GradoopConfiguration, URI OR Path to connect to plus
    // the Implementation of the Neo4j to Gradoop interface
    private GradoopConfig config;
    private String URI;
    private String user;
    private String pw;
    private Reader reader;
    private boolean local;


    /**
     * Constructor for a Local Neo4j database Directory
     * @param path Path to a local Neo4j Database Directory
     * @see GradoopConfig
     * @see LocalReader
     */


    public Neo4jToGradoopReader(String path){
        this.URI = path;
        this.config = new GradoopConfig();
        this.local = true;
        reader = new LocalReader(URI);

    }

    /**
     * Constructor for a Remote Neo4j Database
     * @param Uri Bolt protocol URI for the neo4j database
     * @param User Username for login
     * @param password password for Login
     * @see RemoteReader
     */
    public Neo4jToGradoopReader(String Uri, String User, String password){
        this.URI = Uri;
        this.config = new GradoopConfig();
        RemoteReader reader = new RemoteReader(Uri,User,password);
        this.local = false;
        this.user = User;
        this.pw = password;
        this.reader = reader;

    }

    /**
     * Executes a Cypher Query on this Reader Object, converts it into Gradoop
     * Logical Graph representation
     * @param query The Cypherquery to execute
     * @return The Result of the query in Gradoop format
     */

    public LogicalGraph readGraph(String query){
        try {
            return reader.getGraphFromNeo4j(query, config);
        }catch(Exception e){
                e.printStackTrace();
        }
        return null;
    }

    public void setConfig(GradoopConfig config) {
        this.config = config;
    }


    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    /**
     * Terminates this database connection
     */

    public void close(){
        reader.close();

    }

    public GradoopConfig getConfig() {
        return config;
    }

    public String getUser() {
        return user;
    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public void setUser(String user) {
        this.user = user;
    }




}

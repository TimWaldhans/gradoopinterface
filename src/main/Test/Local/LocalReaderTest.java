package Local;


import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Local.LocalReader;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class LocalReaderTest {


    static LocalReader localReader;
    static String path = "/media/timtpc/SharedData/SharedData/UNI/Bachelorarbeit/GradoopConfig/src/main/java/Test/neo4j-community-3.5.14/data/databases/graph.db";
    static GradoopConfig gradoopInterface;
    static String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    static String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
    static String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";


    @BeforeClass
    public static void setUpDB(){
        gradoopInterface = new GradoopConfig();
        localReader = new LocalReader(path);
        localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");

    }

    public LogicalGraph createGraphFromCollection(GraphCollection gc){
        ReduceCombination rc = new ReduceCombination();
        return (LogicalGraph) rc.execute(gc);
    }



    @Test
    public void readEmptyDB(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            LogicalGraph lg =  localReader.getGraphFromNeo4j("MATCH (n) RETURN *", gradoopInterface);
            lg.print();
            boolean empty =  lg.isEmpty().collect().get(0);
            Assert.assertEquals(true,empty);
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void readNodesOnly(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' }),(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);

            Thread.sleep(1000);
            LogicalGraph lg = localReader.getGraphFromNeo4j("MATCH (n) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });

            String resultGraph =" g1:graph[ (n:Person { name: \"A\" }),(m:Person { name: \"A\" }) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);

            lg.print();
            result.print();


            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));




            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readPlainGraph(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);

            Thread.sleep(1000);
            LogicalGraph lg = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person { name: \"A\" })-[e:E { name: \"E\" }]->(m:Person { name: \"A\" }) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);


            lg.print();
            result.print();



            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readEmptyPropertyGraph(){

        try {
           localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person)-[e:E]->(m:Person) ";
            localReader.getGraphDatabase().execute(create);

            Thread.sleep(1000);
            LogicalGraph lg = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person)-[e:E]->(m:Person) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);

            lg.print();
            result.print();


            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
           localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readMultipleLabelGraph(){
        try {
          localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person:Student)-[e:E]->(m:Person:Student) ";
            localReader.getGraphDatabase().execute(create);

            Thread.sleep(1000);
            LogicalGraph lg =localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person)-[e:E]->(m:Person) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.setLabel("Person Student");
                        return currentVertex;
                    });

            lg.print();
            result.print();


            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }


    @AfterClass
    public static void killDB(){
       localReader.close();

    }


}

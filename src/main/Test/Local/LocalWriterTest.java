package Local;


import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Local.LocalReader;
import Interface.Neo4j.Local.LocalWriter;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class LocalWriterTest {


    static LocalReader localReader;
    static  LocalWriter localWriter;
    static String path = "/media/timtpc/SharedData/SharedData/UNI/Bachelorarbeit/GradoopConfig/src/main/java/Test/neo4j-community-3.5.14/data/databases/graph.db";
    static GradoopConfig gradoopInterface;
    static String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    static String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
    static String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";


    @BeforeClass
    public static void setUpDB(){
        gradoopInterface = new GradoopConfig();
        localReader = new LocalReader(path);
        localWriter = new LocalWriter(localReader.getGraphDatabase());
        localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");


    }


    public LogicalGraph createGraphFromCollection(GraphCollection gc){
        ReduceCombination rc = new ReduceCombination();
        return (LogicalGraph) rc.execute(gc);
    }


    @Test
    public void writeGraphWithoutChanges(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg.copy();
            localWriter.putGraph(lg,lg2);
            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);

            lg.print();
            result.print();


            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void writeNewGraphElements(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            String resultGraph =" g1:graph[ (n:Person)-[e:E { name: \"E\" }]->(m:Person { name: \"B\" }) ]";

            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            LogicalGraph lg2 = loader.getLogicalGraph().combine(lg1);
            localWriter.putGraph(lg1,lg2);
            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);

            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            lg2.print();
            result.print();



            Assert.assertEquals(true,result.equalsByData(lg2).collect().get(0));
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void onlyDeleteEdges(){
        try {
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg1.copy();
            lg2 =  lg2.subgraph(v -> true ,e -> false);
            localWriter.putGraph(lg1,lg2);

            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN n",gradoopInterface);
            int edgeCount = result.getEdges().collect().size();

            Assert.assertEquals(0,edgeCount);
            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void onlyDeleteNodes(){

        try {


            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg1.copy();
            lg2 =  lg2.subgraph(v -> v.getPropertyValue("name").getString().equals("B") ,e -> false);

            localWriter.putGraph(lg1,lg2);
            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n) RETURN *",gradoopInterface);
            result =  result.subgraph(v -> true ,e -> true);

            lg2.print();
            result.print();



            Assert.assertEquals(true,result.equalsByData(lg2).collect().get(0));


            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void writeNewEdgesBetweenExistingElements(){

        try {

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);


            String matchPattern = "MATCH (n:Person)-[e:E]->(m:Person)";
            String creationPattern = "(n)-[e]->(m) "+
                    "(m)<-[e2:E]-(n)";
            LogicalGraph lg2 = createGraphFromCollection(lg1.query(matchPattern,creationPattern));


           localWriter.putGraph(lg1,lg2);
            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            lg2 = lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            lg2.print();
            result.print();



            Assert.assertEquals(true,lg2.equalsByData(result).collect().get(0));

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");

            Thread.sleep(1000);



        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void writeNewEdgesBetweenExistingAndNewElements(){
        try {

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);


            String matchPattern = "MATCH (n:Person)-[e:E]->(m:Person)";
            String creationPattern = " (l:Tier)-[e2:ENEU]->(n)-[e]->(m) " ;
            GraphCollection gc = lg1.query(matchPattern,creationPattern);

            List<EPGMEdge> resultEdges = gc.getEdges().collect();
            List<EPGMVertex> resultVertices = gc.getVertices().collect();
            for(EPGMEdge e : resultEdges){
                for (EPGMVertex v :resultVertices){
                    if(e.getLabel().equals("ENEU")){
                        if(v.getLabel().equals("Person") && v.getPropertyValue("name").equals("A")){
                            e.setTargetId(v.getId());
                        }
                    }
                    if(e.getLabel().equals("ENEU")){
                        if(v.getLabel().equals("Tier")){
                            e.setSourceId(v.getId());
                        }
                    }
                }
            }

            LogicalGraphFactory graphFac = gradoopInterface.getConfig().getLogicalGraphFactory();

            LogicalGraph lg2= graphFac.fromCollections(resultVertices, resultEdges);



            localWriter.putGraph(lg1,lg2);

            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);

            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            result.print();
            lg2.print();

            Assert.assertEquals(true,result.equalsByData(lg2).collect().get(0));

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);



        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @Test
    public void writeMultipleLabeledGraph(){
        try {

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            LogicalGraph lg2 = lg1.copy();


            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty("name");
                        currentVertex.setLabel("Person Test");
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty("name");
                        return currentEdge;
                    });

            localWriter.putGraph(lg1,lg2);

            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);

            lg2.print();
            result.print();

            boolean equals = result.equalsByElementData(lg2).collect().get(0);

            Assert.assertEquals(true,equals);



        }catch(Exception e){
            e.printStackTrace();
        }
    }




    @Test
    public void writeElementsWithNewProperties(){
        try {

            localReader.getGraphDatabase().execute("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            localReader.getGraphDatabase().execute(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            LogicalGraph lg2 = lg1.copy();


            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.setProperty("TESTKNOTEN","ergebnis");
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.setProperty("TESTKANTE","name");
                        return currentEdge;
                    });

            localWriter.putGraph(lg1,lg2);
            LogicalGraph result = localReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);
                        return currentEdge;
                    });
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);
                        return currentEdge;
                    });

            lg2.print();
            result.print();

            Assert.assertEquals(true,result.equalsByData(lg2).collect().get(0));




        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @AfterClass
    public static void killDB(){
        localReader.close();
        localWriter.close();

    }



}

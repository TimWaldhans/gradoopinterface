package Remote;




import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Remote.RemoteReader;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;
import org.junit.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class RemoteReaderTest {


    static RemoteReader remoteReader;
    static GradoopConfig gradoopInterface;
    static String uri_local = "bolt://localhost:7556/";
    static String user = "neo4j";
    static String pw = "1234";
    static String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
    static String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
    static String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";



    @BeforeClass
    public static void setUpDB(){

        Process p;
        try {
            String[] cmd = {"/media/timtpc/SharedData/SharedData/UNI/Bachelor/Bachelorarbeit/gradoopinterface/src/main/Test/Remote/startDB.sh"};
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            Thread.sleep(30000);


        }catch(Exception e){
            e.printStackTrace();
        }
        gradoopInterface = new GradoopConfig();
        remoteReader = new RemoteReader(uri_local, user, pw);
        remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");


    }

    public LogicalGraph createGraphFromCollection(GraphCollection gc){
        ReduceCombination rc = new ReduceCombination();
        return (LogicalGraph) rc.execute(gc);
    }


    @Test
    public void readEmptyDB(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n) RETURN *", gradoopInterface);
            lg.print();
            boolean empty =  lg.isEmpty().collect().get(0);
            Assert.assertEquals(true,empty);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void readNodesOnly(){
        try {
         remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
         Thread.sleep(4000);
         String create = "CREATE (n:Person { name: 'A' }),(m:Person { name: 'A' }) ";
         remoteReader.getDriver().session().run(create);
         Thread.sleep(4000);
         Thread.sleep(1000);
         LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n) RETURN *", gradoopInterface);
         lg =  lg
                 .subgraph(v -> true ,e -> true)
                 .transformVertices((currentVertex, transformedVertex) -> {
                     currentVertex.removeProperty(CYPHER_SOURCE_ID);
                     return currentVertex;
                 });

         String resultGraph =" g1:graph[ (n:Person { name: \"A\" }),(m:Person { name: \"A\" }) ]";
         FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
         loader.initDatabaseFromString(resultGraph);
         GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
         LogicalGraph result = createGraphFromCollection(c1);

            lg.print();
            result.print();
         Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readPlainGraph(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            remoteReader.getDriver().session().run(create);

            Thread.sleep(1000);
            LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person { name: \"A\" })-[e:E { name: \"E\" }]->(m:Person { name: \"A\" }) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);

            lg.print();
            result.print();

            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readEmptyPropertyGraph(){

        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person)-[e:E]->(m:Person) ";
            remoteReader.getDriver().session().run(create);

            Thread.sleep(1000);
            LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person)-[e:E]->(m:Person) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);


            lg.print();
            result.print();

            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Test
    public void readMultipleLabelGraph(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person:Student)-[e:E]->(m:Person:Student) ";
            remoteReader.getDriver().session().run(create);

            Thread.sleep(1000);
            LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg =  lg
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            String resultGraph =" g1:graph[ (n:Person)-[e:E]->(m:Person) ]";
            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            GraphCollection c1 = loader.getGraphCollectionByVariables("g1");
            LogicalGraph result = createGraphFromCollection(c1);
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.setLabel("Person Student");
                        return currentVertex;
                    });

            lg.print();
            result.print();

            Assert.assertEquals(true,result.equalsByData(lg).collect().get(0));
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }


    }


    @AfterClass
    public static void killDB(){
        Process p;
        try {
            String[] cmd = { "/media/timtpc/SharedData/SharedData/UNI/Bachelor/Bachelorarbeit/gradoopinterface/src/main/Test/Remote/stopDB.sh" };
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
            }


        }catch(Exception e){
            e.printStackTrace();
        }

        remoteReader.close();

    }

}

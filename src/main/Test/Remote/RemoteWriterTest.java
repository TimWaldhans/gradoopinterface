package Remote;


import Interface.Neo4j.GradoopConfig;
import Interface.Neo4j.Remote.RemoteReader;
import Interface.Neo4j.Remote.RemoteWriter;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.operators.combination.ReduceCombination;
import org.gradoop.flink.util.FlinkAsciiGraphLoader;
import org.junit.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class RemoteWriterTest {



        static RemoteReader remoteReader;
        static RemoteWriter remoteWriter;
        static GradoopConfig gradoopInterface;
        static String uri_local = "bolt://localhost:7556/";
        static String user = "neo4j";
        static String pw = "1234";
        static String CYPHER_SOURCE_ID = "CYPHER_SOURCE_ID";
        static String CYPHER_TARGET_NODE = "CYPHER_TARGET_NODE";
        static String CYPHER_SOURCE_NODE = "CYPHER_SOURCE_NODE";










    @BeforeClass
    public static void setUpDB(){

        Process p;
        try {
            String[] cmd = {"/media/timtpc/SharedData/SharedData/UNI/Bachelor/Bachelorarbeit/gradoopinterface/src/main/Test/Remote/startDB.sh"};
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }


            Thread.sleep(15000);
            gradoopInterface = new GradoopConfig();
        }catch(Exception e){
            e.printStackTrace();
        }
        remoteReader = new RemoteReader(uri_local, user, pw);
        remoteWriter = new RemoteWriter(uri_local, user, pw);
        remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");

        }

    public LogicalGraph createGraphFromCollection(GraphCollection gc){
            ReduceCombination rc = new ReduceCombination();
            return (LogicalGraph) rc.execute(gc);
        }


    @Test
    public void writeGraphWithoutChanges(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg.copy();
            remoteWriter.putGraph(lg,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);




            boolean equals = result.equalsByData(lg).collect().get(0);

            lg2.print();
            result.print();

            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void writeNewGraphElements(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            String resultGraph =" g1:graph[ (n:Person)-[e:E { name: \"E\" }]->(m:Person { name: \"B\" }) ]";

            FlinkAsciiGraphLoader loader = new FlinkAsciiGraphLoader(gradoopInterface.getConfig());
            loader.initDatabaseFromString(resultGraph);
            LogicalGraph lg2 = loader.getLogicalGraph().combine(lg1);
            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);

            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });


            lg2.print();
            result.print();

            boolean equals = result.equalsByData(lg2).collect().get(0);


            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void onlyDeleteEdges(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg1.copy();
            lg2 =  lg2.subgraph(v -> true ,e -> false);
            remoteWriter.putGraph(lg1,lg2);
            boolean edgeCount = remoteReader.getDriver().
                    session().run("MATCH ()-[e]->() RETURN count(e)").list().get(0).values().get(0).asInt()==0;




            Assert.assertEquals(true,edgeCount);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void onlyDeleteNodes(){

        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            LogicalGraph lg2 = lg1.copy();
            lg2 =  lg2.subgraph(v -> v.getPropertyValue("name").getString().equals("B") ,e -> false);
            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n) RETURN *",gradoopInterface);


            lg2.print();
            result.print();



            boolean equals = result.equalsByData(lg2).collect().get(0);

            Assert.assertEquals(true,equals);

            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void writeNewEdgesBetweenExistingElements(){

        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);

            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'A' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);


            String matchPattern = "MATCH (n:Person)-[e:E]->(m:Person)";
            String creationPattern = "(n)-[e]->(m) "+
                    "(m)<-[e2:E]-(n)";
            LogicalGraph lg2 = createGraphFromCollection(lg1.query(matchPattern,creationPattern));


            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            lg2 = lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });

            boolean equals = lg2.equalsByData(result).collect().get(0);


            lg2.print();

            result.print();


            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);



        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void writeNewEdgesBetweenExistingAndNewElements(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);


            String matchPattern = "MATCH (n:Person)-[e:E]->(m:Person)";
            String creationPattern = " (l:Tier)-[e2:ENEU]->(n)-[e]->(m) " ;
            GraphCollection gc = lg1.query(matchPattern,creationPattern);

            List<EPGMEdge> resultEdges = gc.getEdges().collect();
            List<EPGMVertex> resultVertices = gc.getVertices().collect();
            for(EPGMEdge e : resultEdges){
                for (EPGMVertex v :resultVertices){
                    if(e.getLabel().equals("ENEU")){
                        if(v.getLabel().equals("Person") && v.getPropertyValue("name").equals("A")){
                            e.setTargetId(v.getId());
                        }
                    }
                    if(e.getLabel().equals("ENEU")){
                        if(v.getLabel().equals("Tier")){
                            e.setSourceId(v.getId());;
                        }
                    }
                }
            }

            LogicalGraphFactory graphFac = gradoopInterface.getConfig().getLogicalGraphFactory();

            LogicalGraph lg2 = graphFac.fromCollections(resultVertices, resultEdges);

            remoteWriter.putGraph(lg1,lg2);

            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *",gradoopInterface);

            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result =  result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);

                        return currentEdge;
                    });


            lg2.print();
            result.print();


            boolean equals= lg2.equalsByData(result).collect().get(0);

            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);


        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @Test
    public void writeElementsWithDeletedProperties(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            LogicalGraph lg2 = lg1.copy();


            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty("name");
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty("name");
                        return currentEdge;
                    });
            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            lg2.print();
            result.print();
            boolean equals = result.equalsByData(lg2).collect().get(0);

            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);


        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void writeMultipleLabeledGraph(){
        try {

            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            LogicalGraph lg2 = lg1.copy();


            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty("name");
                        currentVertex.setLabel("Person Test");
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty("name");
                        return currentEdge;
                    });

            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);

            lg2.print();
            result.print();

            boolean equals = result.equalsByData(lg2).collect().get(0);

            Assert.assertEquals(true,equals);



            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);



        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void writeElementsWithNewProperties(){
        try {
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");
            Thread.sleep(1000);
            String create = "CREATE (n:Person { name: 'A' })-[e:E { name: 'E' }]->(m:Person { name: 'B' }) ";
            remoteReader.getDriver().session().run(create);
            Thread.sleep(1000);
            LogicalGraph lg1 = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);


            LogicalGraph lg2 = lg1.copy();


            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.setProperty("TESTKNOTEN","ergebnis");
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.setProperty("TESTKANTE","name");
                        return currentEdge;
                    });
            remoteWriter.putGraph(lg1,lg2);
            LogicalGraph result = remoteReader.getGraphFromNeo4j("MATCH (n)-[e]->(m) RETURN *", gradoopInterface);
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            lg2 =  lg2
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);
                        return currentEdge;
                    });
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformVertices((currentVertex, transformedVertex) -> {
                        currentVertex.removeProperty(CYPHER_SOURCE_ID);
                        return currentVertex;
                    });
            result = result
                    .subgraph(v -> true ,e -> true)
                    .transformEdges((currentEdge, transformedEdge) -> {
                        currentEdge.removeProperty(CYPHER_SOURCE_ID);
                        currentEdge.removeProperty(CYPHER_SOURCE_NODE);
                        currentEdge.removeProperty(CYPHER_TARGET_NODE);
                        return currentEdge;
                    });

            lg2.print();
            result.print();

            boolean equals = result.equalsByData(lg2).collect().get(0);
            Assert.assertEquals(true,equals);
            remoteReader.getDriver().session().run("MATCH (n) DETACH DELETE n");


        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @AfterClass
    public static void tearDown(){

        Process p;
        try {
            String[] cmd = { "/media/timtpc/SharedData/SharedData/UNI/Bachelor/Bachelorarbeit/gradoopinterface/src/main/Test/Remote/stopDB.sh" };
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
            }


        }catch(Exception e){
            e.printStackTrace();
        }
        remoteReader.close();
        remoteWriter.close();
    }





}
